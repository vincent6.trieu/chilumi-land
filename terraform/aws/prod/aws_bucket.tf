resource "aws_s3_bucket" "www_bucket_dt" {
  bucket = var.BUCKET
}

resource "aws_s3_bucket_acl" "www_bucket_dt_acl" {
  bucket = aws_s3_bucket.www_bucket_dt.id
  acl    = "public-read"
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = aws_s3_bucket.www_bucket_dt.id
  policy = file("s3_bucket_policy.json")
}

resource "aws_s3_bucket_website_configuration" "www_bucket_dt" {
  bucket = aws_s3_bucket.www_bucket_dt.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }
}


locals {
  mime_types = {
    html  = "text/html"
    css   = "text/css"
    ttf   = "font/ttf"
    woff  = "font/woff"
    woff2 = "font/woff2"
    js    = "application/javascript"
    map   = "application/javascript"
    json  = "application/json"
    jpg   = "image/jpeg"
    png   = "image/png"
    svg   = "image/svg+xml"
    eot   = "application/vnd.ms-fontobject"
  }
}

resource "aws_s3_bucket_object" "object" {
  for_each     = fileset(path.module, "${var.BUCKET}/**/*")
  bucket       = aws_s3_bucket_website_configuration.www_bucket_dt
  key          = replace(each.value, "${var.BUCKET}", "")
  source       = each.value
  etag         = filemd5("${each.value}")
  content_type = lookup(local.mime_types, split(".", each.value)[length(split(".", each.value)) - 1])
}

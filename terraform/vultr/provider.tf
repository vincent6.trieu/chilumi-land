terraform {
  required_providers {
    vultr = {
      source  = "vultr/vultr"
      version = ">= 2.10.1"
    }
  }
}

provider "vultr" {
  api_key     = var.VULTR_API_KEY
  rate_limit  = var.RATE_LIMIT
  retry_limit = var.RETRY_LIMIT
}

variable "VULTR_API_KEY" {}
variable "PLAN" {}
variable "RATE_LIMIT" {}
variable "RETRY_LIMIT" {}
variable "OS_ID" {}
variable "REGION" {}


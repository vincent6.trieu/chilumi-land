
resource "vultr_instance" "vultr_instance_dt" {
  plan        = var.PLAN
  region      = var.REGION
  os_id       = var.OS_ID
  ssh_key_ids = ["${data.vultr_ssh_key.primary.id}"]
  enable_ipv6 = true
  hostname    = "instance"
  #  startup_script_id = "ubuntu-ansible-ready"
  tags              = ["ubuntu20"]
  firewall_group_id = vultr_firewall_group.base-group.id
}

# output "public_ip_address" {
#   value = vultr_instance.vultr_instance_dt.main_ip
# }

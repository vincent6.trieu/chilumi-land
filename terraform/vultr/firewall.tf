# Create a new firewall group.
resource "vultr_firewall_group" "base-group" {
  description = "base group"
}

# SSH
# Add a firewall rule to the group allowing SSH access.
resource "vultr_firewall_rule" "ssh" {
  firewall_group_id = vultr_firewall_group.base-group.id
  protocol          = "tcp"
  ip_type           = "v4"
  subnet            = "0.0.0.0"
  subnet_size       = 0
  port              = "8090"
  notes             = "my firewall rule"
}

# PING
# Add a firewall rule to the group allowing ICMP.
resource "vultr_firewall_rule" "icmp" {
  firewall_group_id = vultr_firewall_group.base-group.id
  ip_type           = "v4"
  subnet            = "0.0.0.0"
  subnet_size       = 0
  protocol          = "icmp"
}

# MUMBLE TCP
resource "vultr_firewall_rule" "base-group-tcp" {
  firewall_group_id = vultr_firewall_group.base-group.id
  ip_type           = "v4"
  subnet            = "0.0.0.0"
  subnet_size       = 0
  protocol          = "tcp"
  port              = 64738
}

# MUMBLE UDP
resource "vultr_firewall_rule" "base-group-udp" {
  firewall_group_id = vultr_firewall_group.base-group.id
  ip_type           = "v4"
  subnet            = "0.0.0.0"
  subnet_size       = 0
  protocol          = "udp"
  port              = 64738
}

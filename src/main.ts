import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ResponseInterceptor } from '@/shared/interceptors/http-response.interceptor';
import { configService } from './config/services.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  app.useGlobalInterceptors(new ResponseInterceptor());
  if (!configService.isProduction()) {
    const document = SwaggerModule.createDocument(app, new DocumentBuilder().setTitle('Doc API').setDescription('My Doc API').build());

    SwaggerModule.setup('docs', app, document);
  }

  await app.listen(3000);
}
bootstrap();

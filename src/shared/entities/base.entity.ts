import { Injectable } from '@nestjs/common';
import { Column, BaseEntity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Injectable()
export class BaseEntityClass extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({
    name: 'created_date_time',
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdDateTime: Date;

  @Column({
    name: 'created_by',
    type: 'varchar',
    length: 300,
    nullable: true,
    default: 'admin',
  })
  createdBy: string;

  @UpdateDateColumn({
    name: 'last_changed_date_time',
    type: 'timestamptz',
    default: () => 'CURRENT_TIMESTAMP',
  })
  lastChangedDateTime: Date;

  @Column({
    name: 'last_changed_by',
    type: 'varchar',
    length: 300,
    nullable: true,
  })
  lastChangedBy: string;
}

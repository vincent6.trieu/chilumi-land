export interface FilterQueryOptions {
  queryCriteria?: { [P in any | string]: any };
  select?: any[];
  populate?: any[];
  sort?: any;
}

export enum ORDER_BY {
  ASC = 'ASC',
  DESC = 'DESC',
}

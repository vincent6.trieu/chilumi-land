import { RESPONSE_CODE, RESPONSE_MESSAGE } from '../constants/http-message.constants';
import { Injectable, Logger } from '@nestjs/common';
import { DeleteResult, EntityTarget, FindManyOptions, InsertResult, FindOneOptions, BaseEntity, UpdateResult, FindOptionsWhere } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { paginate, Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';
import { FilterQueryOptions } from '../types';
import { ResponseData } from '../helpers/response.helper';
import { BaseRepository } from '../repositories/base.repo';

@Injectable()
export class BaseService<Entity extends BaseEntity> {
  constructor(protected repository: BaseRepository<Entity>, private readonly _entity: EntityTarget<Entity>, private readonly _serviceName: string) {}

  private readonly logger = new Logger(`${this._serviceName}`);

  async create(entity: QueryDeepPartialEntity<Entity>): Promise<ResponseData<InsertResult>> {
    try {
      this.logger.log(this.create.name + ' : ' + JSON.stringify(entity));
      const result = await this.repository.insert(entity);

      return new ResponseData({
        data: result,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.CREATE_SUCCEED],
      });
    } catch (error) {
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    }
  }

  async updateById(id: number, updateData: QueryDeepPartialEntity<Entity>): Promise<ResponseData<UpdateResult>> {
    try {
      this.logger.log(this.updateById.name + ':' + JSON.stringify(updateData));

      const result = await this.repository.update(id, updateData);

      return new ResponseData({
        data: result,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.UPDATE_SUCCEED],
      });
    } catch (error) {
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    }
  }

  async update(queryCriteria: FindOptionsWhere<Entity>, updateData: QueryDeepPartialEntity<Entity>): Promise<ResponseData<UpdateResult>> {
    try {
      const result = await this.repository.update(queryCriteria, updateData);

      return new ResponseData({
        data: result,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.UPDATE_SUCCEED],
      });
    } catch (error) {
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    }
  }

  async deleteAll(): Promise<ResponseData<DeleteResult>> {
    // Get the queryRunner from default transaction and connect to database
    const queryRunner = this.repository.manager.connection.createQueryRunner();
    await queryRunner.connect();

    try {
      // begin transaction
      await queryRunner.startTransaction();

      const signal = await queryRunner.manager.delete(this._entity, {});

      // commit all to database
      await queryRunner.commitTransaction();

      return new ResponseData({
        data: signal,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.DELETE_SUCCEED],
      });
    } catch (error) {
      // since we have errors let's rollback changes we made
      this.logger.log('error: ', error);
      await queryRunner.rollbackTransaction();
      this.logger.log('Transaction rollback');
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    } finally {
      // you need to release query runner which is manually created:
      this.logger.log('Transaction released');
      await queryRunner.release();
    }
  }

  async delete(deleteCriteria: FindManyOptions = {}): Promise<ResponseData<DeleteResult>> {
    this.logger.log(this.delete.name + ': ' + JSON.stringify(deleteCriteria));

    // Get the queryRunner from default transaction and connect to database
    const queryRunner = this.repository.manager.connection.createQueryRunner();
    await queryRunner.connect();

    try {
      // begin transaction
      await queryRunner.startTransaction();

      const signal = await queryRunner.manager.delete(this._entity, deleteCriteria);

      // commit all to database
      await queryRunner.commitTransaction();

      return new ResponseData({
        data: signal,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.DELETE_SUCCEED],
      });
    } catch (error) {
      // since we have errors let's rollback changes we made
      this.logger.log('error: ', error);
      await queryRunner.rollbackTransaction();
      this.logger.log('Transaction rollback');
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    } finally {
      // you need to release query runner which is manually created:
      this.logger.log('Transaction released');
      await queryRunner.release();
    }
  }

  async findAll(queryCriteria: FindManyOptions<Entity> = {}): Promise<ResponseData<Entity[]>> {
    try {
      this.logger.log(this.findAll.name + ': ' + JSON.stringify(queryCriteria));
      const rs = await this.repository.find(queryCriteria);
      return new ResponseData({
        data: rs,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.GET_SUCCEED],
      });
    } catch (error) {
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    }
  }

  async findOne(queryCriteria: FindOneOptions = {}): Promise<ResponseData<Entity>> {
    try {
      this.logger.log(this.findOne.name + ': ' + JSON.stringify(queryCriteria));
      const rs = await this.repository.findOne(queryCriteria);
      return new ResponseData({
        data: rs,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.GET_SUCCEED],
      });
    } catch (error) {
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    }
  }

  async find({ queryCriteria, select = [], populate = [], sort = {} }: FilterQueryOptions = {}): Promise<ResponseData<Entity>> {
    try {
      this.logger.log(this.find.name + ' ' + JSON.stringify(queryCriteria));

      const rs = await this.repository.findOne({
        where: queryCriteria,
        select,
        relations: populate,
        order: sort,
      });

      return new ResponseData({
        data: rs,
        message: RESPONSE_MESSAGE[RESPONSE_CODE.GET_SUCCEED],
      });
    } catch (error) {
      this.logger.error(error);
      return new ResponseData({
        error: true,
        message: error.message,
      });
    }
  }

  async paginate(options: IPaginationOptions, { queryCriteria, select, populate, sort }: FilterQueryOptions): Promise<Pagination<Entity>> {
    this.logger.log(this.paginate.name);

    const searchOptions: FindManyOptions = {};

    if (select) searchOptions.select = select;
    if (sort) searchOptions.order = sort;
    if (queryCriteria) searchOptions.where = queryCriteria;
    if (populate) searchOptions.relations = populate;

    return paginate<Entity>(this.repository, options, searchOptions);
  }
}

export const ERROR_CODE = {
  NOT_FOUND: 'not_found',
  FILE_NOT_PROVIDED: 'file_not_provided',
  INVALID_PAGINATION_INPUT: 'invalid_pagination_input',
};

export const ERROR_MESSAGE = {
  [ERROR_CODE.NOT_FOUND]: 'Not found',
  [ERROR_CODE.FILE_NOT_PROVIDED]: 'File not provided',
  [ERROR_CODE.INVALID_PAGINATION_INPUT]: 'Invalid pagination input',
};

export const RESPONSE_CODE = {
  GET_SUCCEED: 'get_succeed',
  DELETE_SUCCEED: 'delete_succeed',
  UPDATE_SUCCEED: 'update_succeed',
  CREATE_SUCCEED: 'create_succeed',
};

export const RESPONSE_MESSAGE = {
  [RESPONSE_CODE.GET_SUCCEED]: 'Get succeed',
  [RESPONSE_CODE.DELETE_SUCCEED]: 'Delete succeed',
  [RESPONSE_CODE.UPDATE_SUCCEED]: 'Update succeed',
  [RESPONSE_CODE.CREATE_SUCCEED]: 'Create succeed',
};

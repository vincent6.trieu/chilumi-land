import { ORDER_BY } from './../types/index';
import {
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  HttpCode,
  Query,
  Put,
  Logger,
  CacheInterceptor,
  UseInterceptors,
  CacheTTL,
  CACHE_MANAGER,
  Inject,
} from '@nestjs/common';
import { Cache } from 'cache-manager';
import { BaseEntity, FindManyOptions } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { BaseService } from '../services/base.service';

@UseInterceptors(CacheInterceptor)
@CacheTTL(30)
export class BaseController<T extends BaseEntity> {
  protected readonly service: BaseService<T>;
  private readonly prefix: string;
  @Inject(CACHE_MANAGER) private cacheService: Cache;

  constructor(private readonly _service: BaseService<T>, private readonly _controllerName: string) {
    this.service = _service;
    this.prefix = _controllerName;
  }

  public readonly logger = new Logger(`${this._controllerName}`);

  @Post()
  public async create(@Body() entity: QueryDeepPartialEntity<T>) {
    this.logger.log(this.create.name + ': ' + JSON.stringify(entity));
    const signal = await this._service.create(entity);

    if (signal.error) return new HttpException(signal.message, signal.httpCode);

    return signal;
  }

  @Put()
  public async update(@Query() query, @Body() entity: QueryDeepPartialEntity<T>) {
    this.logger.log(this.update.name);
    const signal = await this._service.update(query, entity);

    if (signal.error) return new HttpException(signal.message, signal.httpCode);

    return signal;
  }

  @Put('id/:id')
  public async updateById(@Param() id: number, @Body() entity: QueryDeepPartialEntity<T>) {
    const signal = await this._service.updateById(id, entity);
    if (signal.error) return new HttpException(signal.message, signal.httpCode);

    return signal;
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  public async findAll(@Query() query: any) {
    const { limit, skip, select, orderBy = ORDER_BY.DESC, sortBy = 'createdDateTime', ...filter } = query;
    const queryCriteria: FindManyOptions = {
      where: filter,
      order: { [sortBy]: orderBy },
    };

    if (skip) queryCriteria.skip = skip;
    if (limit) queryCriteria.take = limit;
    if (select) queryCriteria.select = typeof select === 'string' ? select.split(' ') : select;

    const signal = await this._service.findAll(queryCriteria);
    if (signal.error) return new HttpException(signal.message, signal.httpCode);

    return signal;
  }

  @Get('id/:id')
  @HttpCode(HttpStatus.OK)
  public async findById(@Param('id') id: number) {
    this.logger.log(this.findById.name);
    const key = this.prefix + '_' + id.toString();

    const cachedData = await this.cacheService.get<{ name: string }>(key);
    if (cachedData) {
      console.log(`Getting data from cache!`);
      return cachedData;
    }

    const signal = await this._service.find({ queryCriteria: { id }, sort: { createdDateTime: ORDER_BY.DESC } });

    if (signal.error) return new HttpException(signal.message, signal.httpCode);
    await this.cacheService.set(key, signal);
    return signal;
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  async removeAll() {
    const signal = await this._service.deleteAll();
    if (signal.error) return new HttpException(signal.message, signal.httpCode);

    return signal;
  }

  @Delete('id/:id')
  @HttpCode(HttpStatus.OK)
  async removeById(@Param('id') id: number) {
    const signal = await this._service.delete({
      where: { id },
    });

    if (signal.error) return new HttpException(signal.message, signal.httpCode);

    return signal;
  }
}

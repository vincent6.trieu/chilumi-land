import { Logger } from '@nestjs/common';
import { writeFile, mkdirSync, existsSync, readFile } from 'fs';

export class FileUtils {
  private readonly logger = new Logger(FileUtils.name);

  async createFile(fileDirPath: string, imageName: string, data: any) {
    if (!existsSync(fileDirPath)) {
      mkdirSync(fileDirPath);
    }

    return writeFile(fileDirPath + '/' + imageName, data, function (err) {
      if (err) {
        console.log(err);
        throw err;
      }
      console.log('Asynchronous write success');
    });
  }

  async showFile(filePath: string) {
    return readFile(filePath, function (err, data) {
      if (err) {
        return console.log(err);
      }
      console.log('Asynchronous read');
      return data;
    });
  }
}

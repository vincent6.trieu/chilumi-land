import { BadRequestException, Controller, HttpStatus, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BaseController } from '@/shared/controllers/base.controller';
import { Image } from '../entities/image.entity';
import { ImagesService } from '../services/images.service';
import { BASE_FILE_DIR_IMAGE } from '../../shared/constants/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ResponseData } from '@/shared/helpers/response.helper';
import { ERROR_CODE, ERROR_MESSAGE, RESPONSE_CODE, RESPONSE_MESSAGE } from '@/shared/constants/http-message.constants';
import { ReturnController } from 'src/shared/interceptors/http-response.interceptor';

@Controller('images')
@ApiTags('Admin Image')
export class ImageController extends BaseController<Image> {
  constructor(imageService: ImagesService) {
    super(imageService, ImageController.name);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('image', { dest: './src/public/uploads/images' }))
  async upload(@UploadedFile() file: any): Promise<ReturnController<any>> {
    if (!file)
      throw new BadRequestException(
        new ResponseData({
          error: true,
          message: ERROR_MESSAGE[ERROR_CODE.FILE_NOT_PROVIDED],
          data: file,
          errorKey: ERROR_CODE.FILE_NOT_PROVIDED,
          httpCode: HttpStatus.BAD_REQUEST,
        }),
      );

    //save file information to the database
    await this.service.create({
      name: file.originalname,
      alt: file.originalname,
      src: `${BASE_FILE_DIR_IMAGE}/${file.filename}`,
    });

    return {
      message: RESPONSE_MESSAGE[RESPONSE_CODE.UPDATE_SUCCEED],
      data: file,
      httpCode: HttpStatus.CREATED,
    };
  }
}

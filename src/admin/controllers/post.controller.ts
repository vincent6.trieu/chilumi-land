import { Post } from './../entities';
import { Controller } from '@nestjs/common';
import { BaseController } from '@/shared/controllers/base.controller';
import { ApiTags } from '@nestjs/swagger';
import { PostService } from '../services/posts.service';

@Controller('posts')
@ApiTags('Admin Post')
export class PostController extends BaseController<Post> {
  constructor(postService: PostService) {
    super(postService, PostController.name);
  }
}

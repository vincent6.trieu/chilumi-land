import { Controller, Post, Req, Next, Body, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AdminService } from '../services/admins.service';
import { NextFunction, Request } from 'express';

@Controller()
export class LoginController {
  private readonly adminService: AdminService;

  @Post()
  refillPassword(@Req() req: Request, @Next() next: NextFunction) {
    if (!req.body.password) {
      req.body.password = '   ';
    }
    return next();
  }

  @Post('login')
  @UseGuards(AuthGuard('local'))
  async login(@Req() req: any) {
    req.session.user = req.user;
    if (!req.user) {
      return false;
    }
    return true;
  }

  @Post('register')
  async register(@Body() body) {
    return this.adminService.register(body);
  }

  async destroySession(@Req() req: any) {
    req.logout();
    req.session.destroy();
    return true;
  }
}

import { BaseEntityClass } from '@/shared/entities/base.entity';
import { Column, Entity, ManyToMany, OneToMany } from 'typeorm';
import { PageComponent } from './pageComponent.entity';
import { PostCategory } from './postCategory.entity';

@Entity('Image')
export class Image extends BaseEntityClass {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  alt: string;

  @Column({ type: 'varchar' })
  src: string;

  @ManyToMany(() => PageComponent, pageComponent => pageComponent.images)
  pageComponents!: PageComponent[];

  @OneToMany(() => PostCategory, postCategory => postCategory.banner || postCategory.thumnail)
  postCategories: PostCategory[];
}

import { BaseEntityClass } from '@/shared/entities/base.entity';
import { Column, Entity, Index, JoinTable, ManyToMany } from 'typeorm';
import { IsNotEmpty, IsString } from 'class-validator';
import { Image } from './image.entity';
import { IsFiles, MaxFileSize } from 'nestjs-form-data';
import { Doc } from './doc.entity';

@Entity('PageComponent')
export class PageComponent extends BaseEntityClass {
  @Column()
  @IsString()
  @IsNotEmpty()
  @Index()
  name: string;

  @Column({ name: 'page' })
  @IsString()
  @IsNotEmpty()
  @Index()
  page: string;

  @Column({ name: 'seo_schema' })
  seoSchema: string;

  @Column({ name: 'seo_canonical' })
  seoCanonical: string;

  @Column('text', { array: true })
  titles: string[];

  @Column('text', { array: true })
  contents: string[];

  @ManyToMany(() => Image, image => image.pageComponents)
  @JoinTable({
    name: 'image_pageComponent',
    joinColumn: { name: 'imageId', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'pageComponentId', referencedColumnName: 'id' },
  })
  @IsFiles()
  @MaxFileSize(1e6)
  images: Image[];

  @ManyToMany(() => Doc, doc => doc.pageComponents)
  @JoinTable({
    name: 'doc_pageComponent',
    joinColumn: { name: 'docId', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'pageComponentId', referencedColumnName: 'id' },
  })
  @IsFiles()
  @MaxFileSize(1e6)
  docs: Doc[];
}

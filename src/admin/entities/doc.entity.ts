import { BaseEntityClass } from '@/shared/entities/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { DOC_TYPES } from '../enum';
import { PageComponent } from './pageComponent.entity';

@Entity('Doc')
export class Doc extends BaseEntityClass {
  @Column()
  name: string;

  @Column({ type: 'varchar' })
  src: string;

  @Column({ type: 'varchar' })
  type: DOC_TYPES;

  @OneToMany(() => PageComponent, pageComponent => pageComponent.docs)
  pageComponents!: PageComponent[];
}

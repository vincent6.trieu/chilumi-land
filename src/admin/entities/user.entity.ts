import { BaseEntityClass } from '@/shared/entities/base.entity';
import { Column, Entity, Index } from 'typeorm';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

@Entity('User')
export class User extends BaseEntityClass {
  @Column({ name: 'user_name' })
  @IsString()
  @IsNotEmpty()
  @Index({ unique: true })
  username: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ name: 'full_name', type: 'varchar' })
  fullName: string;

  @Column({ name: 'address', type: 'varchar' })
  address: string;

  @Column({ name: 'phone', type: 'varchar' })
  phone: string;

  @Column({ name: 'occupation ', type: 'varchar' })
  occupation: string;

  @Column({ type: 'varchar', length: 100 })
  @IsEmail()
  email: string;
}

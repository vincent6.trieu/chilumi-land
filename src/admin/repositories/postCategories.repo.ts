import { BaseRepository } from '@/shared/repositories/base.repo';
import { PostCategory } from '@/admin/entities';

export class PostCategoryRepository extends BaseRepository<PostCategory> {}

import { BaseRepository } from '@/shared/repositories/base.repo';
import { Admin } from '@/admin/entities';

export class AdminRepository extends BaseRepository<Admin> {}

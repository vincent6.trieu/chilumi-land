import { AdminService } from '../services/admins.service';
import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private adminService: AdminService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    const signalLogin = await this.adminService.login({ username, password });
    if (!signalLogin) {
      throw new UnauthorizedException();
    }
    return signalLogin;
  }
}

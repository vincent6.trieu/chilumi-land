import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const { session } = request;
    console.log({ session });
    if (session && session.user && session.user.role == 2) {
      return true;
    }
    return false;
  }
}

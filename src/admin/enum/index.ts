export enum USER_ROLE {
  EDITOR = 0,
  SEOER = 1,
  ADMIN = 2,
}

export enum DOC_TYPES {
  DOC = 'doc',
  PDF = 'pdf',
}

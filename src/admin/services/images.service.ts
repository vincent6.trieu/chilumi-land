import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '@/shared/services/base.service';
import { Image } from '../entities/image.entity';
import { ImageRepository } from '../repositories/image.repo';

@Injectable()
export class ImagesService extends BaseService<Image> {
  constructor(
    @InjectRepository(Image)
    imageRepository: ImageRepository,
  ) {
    super(imageRepository, Image, ImagesService.name);
  }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '@/shared/services/base.service';
import { Post } from '../entities';
import { PostRepository } from '../repositories/posts.repo';

@Injectable()
export class PostService extends BaseService<Post> {
  constructor(
    @InjectRepository(Post)
    userRepository: PostRepository,
  ) {
    super(userRepository, Post, PostService.name);
  }
}

import { DataSource } from 'typeorm';
import 'dotenv/config';
import { configService } from '@/config/services.config';
import { join } from 'path';
import { init1670043824187 } from '@/migrations';

const listMigrations = [init1670043824187];

export default new DataSource({
  type: 'postgres',
  host: configService.getValue('POSTGRES_HOST'),
  port: parseInt(configService.getValue('POSTGRES_PORT')),
  username: configService.getValue('POSTGRES_USER'),
  password: configService.getValue('POSTGRES_PASSWORD'),
  database: configService.getValue('POSTGRES_DATABASE'),
  entities: [join(__dirname, '**', '*.entity.{ts,js}')],
  migrationsTableName: 'migrations',
  migrations: listMigrations,
  ssl: configService.isProduction(),
});
